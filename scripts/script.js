cookieValue = cookie => {
    return document.cookie
        .split('; ')
        .find((row) => row.startsWith(`${cookie}`))
        ?.split('=')[1];
}

onAdultContentCheckboxClicked = function(e) {
    if (e.checked) {
        //document.cookie="donotfilter=true"
        showFilteredElements();
    } else {
        //document.cookie="donotfilter=false"
        hideFilteredElements();
    }
}

showFilteredElements = () => {
    const elements = document.querySelectorAll(".filtered");
    for (var i = 0; i < elements.length; i++) {
        elements.item(i)?.classList?.replace('filtered', 'unfiltered');
    }
}

hideFilteredElements = () => {
    const elements = document.querySelectorAll(".unfiltered");
    for (var i = 0; i < elements.length; i++) {
        elements.item(i)?.classList?.replace('unfiltered', 'filtered');
    }
}

window.onload = function() {
    /*
    const value = cookieValue('donotfilter');
    if (value === 'true') {
        showFilteredElements();
    }

    const contentFilterCheckbox = document.getElementById('adult_content_checkbox')
    if (contentFilterCheckbox) {
        contentFilterCheckbox.checked = (value === 'true')
    }
    */
}