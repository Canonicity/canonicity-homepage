let systemInitiatedDark = window.matchMedia('(prefers-color-scheme: dark)'); 
let theme = sessionStorage.getItem('theme');
let orbit = document.getElementById('theme-toggle-orbit');

if (systemInitiatedDark.matches) {
	orbit.className = 'light';
} else {
	orbit.className = 'dark';
}

function prefersColorTest(systemInitiatedDark) {
  if (systemInitiatedDark.matches) {
  	document.documentElement.setAttribute('data-theme', 'dark');		
    orbit.className = 'light';
	document.getElementById('theme');
   	sessionStorage.setItem('theme', '');
  } else {
  	document.documentElement.setAttribute('data-theme', 'light');
    orbit.className = 'dark';
    sessionStorage.setItem('theme', '');
  }
}
systemInitiatedDark.addListener(prefersColorTest);


function modeSwitcher() {
	let theme = sessionStorage.getItem('theme');
	if (theme === "dark") {
		animateLight();
	}	else if (theme === "light") {
		animateDark();
	} else if (systemInitiatedDark.matches) {	
		animateLight();
	} else {
		document.documentElement.setAttribute('data-theme', 'dark');
		sessionStorage.setItem('theme', 'dark');
		orbit.className = 'light';
	}
}

function animateLight() {
	document.documentElement.setAttribute('data-theme', 'light');
	sessionStorage.setItem('theme', 'light');
	orbit.className = 'dark-animated';
}

function animateDark() {
	document.documentElement.setAttribute('data-theme', 'dark');
	sessionStorage.setItem('theme', 'dark');
	orbit.className = 'light-animated';
}

if (theme === "dark") {
	document.documentElement.setAttribute('data-theme', 'dark');
	sessionStorage.setItem('theme', 'dark');
	orbit.className = 'light';
} else if (theme === "light") {
	document.documentElement.setAttribute('data-theme', 'light');
	sessionStorage.setItem('theme', 'light');
	orbit.className = 'dark';
}