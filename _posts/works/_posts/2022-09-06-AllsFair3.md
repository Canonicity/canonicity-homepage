---
layout: post

title: Bet the House

author: Sisyphus

category: allsfair

slug: act_3
order: 3

---

When you got home, sleep didn’t come easily.

The next day finds you practically dead on your feet.  But, chores must be done and errands must be run.  Even if life seems grey and empty.  The house feels cold when you do the dishes, lonely when you bag up the trash, and when you leave through the front door to take the garbage out… you almost don’t want to return.

When you move to make your way back up the drive, the world shifts beneath your feet. 

His broad frame fills up your small porch, but all you can see is the deep set of his frown.

Squaring your shoulders, you holler, “Leonan Intara!  This is my property and I demand you clear out!”

Responding by crossing his arms, his weight shifts back on his heels like he’s digging in and going to try standing his ground.  Any thoughts of leaving for good are shoved to the wayside as you’re filled with unaccountable fury.

“This is my home,” you stomp your way to the house, “you’ve no right to be here.”  You storm up the steps, it’s not in your nature to give in so you won’t cower.  “Get the hell away before I have a mind to—”

One substantial exhalation leaves you speechless when his arms constrict around your middle to lift you off your feet.  He carries you over his shoulder and through the open door, only taking time to slam it shut with one boot-clad foot before dropping you unceremoniously onto a nearby loveseat.  You sputter, you’ve never seen Leo behave this way before.

He’s pacing in front of you and has your full attention, but you’re not scared.  “You fuc- ya goddam- shoot!”  

Avoiding curse words is endearing, especially when you know how easily he can flip that switch with his cock in your mouth.  He cards a hand through his hair, you watch and wish you had done that for yourself last night.  Touched him with care.

“Excuse the language, but yer a b-b-,” he breathes deeply, “a brat!”  The exclamation seems to fortify him, but you’re still biting back an inadvertent smile.  What he says next has all your mirth receding.  “You never wrote, you wouldn’t answer the danged phone; I even got Ma’s ring!  I had tah leave, make something of myself, so I could take care of you and so you’d see- so you’d see *me*.”  

He stops his stomping to squeeze his eyes shut and scrub his right hand down his face.  You almost reach out to him, want to grab his left hand to bring him to you, but you think better of it.

Abruptly, he trumpets, “I wouldn’t have left without a plan to come back!”  Then, he drops to his knees in front of you, and your breath stalls in your chest.  “Heck, I *tried* to take you with me, but you wouldn’t even talk to me when I admitted that I—” 

He cuts off abruptly, shoulders falling, not looking at you.

“That night… and last night... they meant something to me,” he whispers with a gravity that has the earth tilting on its axis.  Head lifting up, he peers at you with big, sad eyes.  “It means *everything* to me.”

The definitive statement is met with a heavy silence before you realize he’s waiting for a response.  Sitting there with your mouth open in dumbfounded astonishment, you only notice after your jaw snaps shut.  What’re you supposed to say? 

“I’m not the settlin’ type,” falls from your mouth before you can even *think*.

Eyes closing and leaning forward, he presses his forehead against yours.  “That’s fine, but I can’t have you coming ‘round anymore.  I just- I really can’t.”

Words so quiet shouldn’t fill the room to the point you’re suffocating.

“But—”

His eyes open to meet yours, “No buts, a brat doesn’t get everything she wants.”

He’s going to stand up, he’s going to leave, and you can’t forget the first time he told you he was leaving.  Without warning, you launch yourself at him and tackle him to the ground with your arms slung carelessly around his neck.  You’re showering him in kisses as tears stream down your face.  

“Darlin’, no, it’s all or nothing,” he objects.

Through the frenzy, you sniffle out, “I want it all.  All of you.  All the time.  I don’t want to be a brat anymore, I want to be good for you.”  Not fully conscious of what you’re confessing, you proclaim, “Please stay with me, Leo!”

Grabbing your shoulders, he stops you to confirm, “No foolin’, are you just playin’ around?”

Both of you are searching each other’s eyes as you attempt to formulate what you want to say.  

“I don’t think so, I don’t know.”  

He sighs.  “You’ve a lotta hell to pay.  No sense in not following through.”

The words tumble out, “But I want to try!  I can try to be good for you!  I mean, I *will* try.”  You hesitate, afraid of this *something* so long unspoken.  Something you’ve never admitted to yourself, let alone *him*.  “I’ll try if you let me.”

The strength behind his piercing gaze has you shivering.  It’s like he’s weighing the options, calculating the best way to deal with you, planning your downfall.  His expression hardens, causing you to break out in a cold sweat while you nervously pick at your cuticles.  Eyes never breaking contact with yours, he begins to remove his prosthesis.  

Leo startles you when he darkly decrees, “Get undressed n’ wait fer me right here, on your hands and knees.”  You’re about to object, but he beats you to it.  “No back-talk; brats talk back and brats get punished.”

Appearing indifferent, he makes his way out of the room.  You hear him messing around in the kitchen, drawers opening and closing, metal utensils crashing, a chair scraping across the linoleum.  You’re frozen in place when he surreptitiously peers through the doorway, shirtless.

One of his eyebrows arcs; you’ve been caught.  Holy shit.  As he disappears back into the kitchen you jump into action, ripping your shirt over your head and tugging off your sleep shorts.  

A deep, disembodied voice amends, “Keep the socks.”

You have no idea what he has in store, but you keep telling yourself that this will be worth it.  Tossing every article of clothing behind the loveseat excluding your baby-pink striped knee-socks, of course, you collapse on all fours.  Panting and needy, you can’t hear him anymore.  The silence is all-consuming, so you look up in the hopes to spot him.

The loud, authoritative clearing of a throat has your head bowing.  You’ve never done anything like this before.  Since when have you ever listened to anyone else?  Followed anyone’s orders?  You don’t know what to expect, you’re thrilled and horrified by your own behavior.  Then, you wonder if Leo does this often.  Does he know what he’s doing?  What in the fuck *is* he doing?

“Keep your head down,” he commands, striding into the room.

You can only see his adorably pigeon-toed feet as he circles you while you grovel on the floor at his mercy.  

“This hurts me more than it hurts you.”

You’re not sure what he’s talking about, could be about how you’ve treated him or mayb—

A sharp shock catapults you forward, accompanied by a loud *crack*.  Then, you feel a rising tingle travel from the skin on the back of your knees to your lower back.  A burning spreads across the right cheek of your ass with no singular, discernable point of origin.

Bolting upright, your hands instinctively jump to soothe the hit to your behind, rubbing vigorously.

What the fuck was *that*?  Your head on a swivel, you quickly spy him over your shoulder.  The glare you shoot his way is met with a disapproving scowl.  What astounds you in that moment, more than anything else, is how much you absolutely *hate* disappointing Leo.  

You’re trying to focus instead on how *hard* he hit you the very first time, but he doesn’t wait to scold you, “Gonna be a problem for you, brat?”  Falling back gracefully, he sinks into the couch.  “If you’re gonna be a shit about it, then climb on my lap.”  

The appearance of his filthy mouth excites you, it means he’s losing control.  And, maybe you’ll be able to take him over the moment he gives in.  Would you though, if you could?  Wouldn’t it be more exciting and new to let him have you.  The thought of him overpowering you, conquering you, breaking you until you bend to his impenetrable will, causes your nipples to harden and cunt to clench.

Instead of any of these things, he calmly indicates where he wants you.  Him patting the top of his thigh should *not* look so mouthwateringly inviting.  Not for you.  Still, you scramble towards him before scaling up his muscular legs to drape yourself over his lap.  

He puffs out a quiet chuckle.  “So eager to be punished.  How many you think you’ve earned?  Hundreds?  Thousands, by my count.”  

That gives you pause, you never meant any harm… or maybe you did.  You can’t be sure, you’ve always been a magnet for trouble.  Either way, people change, right?  Shouldn’t it count for something that you're here with him now?  You even apologized!  At least, you think you did…

He doesn’t really allot time for you to order your thoughts.  In fact, the way he lifts you up in the middle using one forearm before trapping your legs under one of his meaty thighs while pushing your head into the cushion outside his other thigh has your mind screaming in chaos.  You’re trapped in his hold, and even if you felt at his mercy before—

“You’re gonna thank me for each and every one.”  The statement is so matter-of-fact, you can’t really object.  “We ain’t gonna stop til I’m satisfied.  Then I’ll *think* about giving you what you want.”

“Leo,” you murmur, unsure of what you want to say.

No matter, he doesn’t allow you to continue.

“No,” he snorts, pressing your head deeper into the cushion so you can feel the weave of the fabric biting into your cheek, “something else.”  His right hand teases the seam of your ass, from clit to pucker, just a light trailing of fingertips that leaves you on tenterhooks.  “What’d you used to call me?”

“Baby Bear,” you automatically gasp out.

“Hmmm…”  It sounds like a rumbling darkness, vibrating its way into sinew and bone.

He wants to be called something else, something more like the man he is now.  More like himself, if you could only *see* him.  If only you could *see*—

Turning your head into the cushion, you quietly offer, “Papa Bear?”

“That’ll do.”  His right hand resolutely squeezes your ass before slipping up your spine to tangle your hair in a knot.  Pulling to stretch your neck, he asks, “Want me to be your daddy, huh?”

Thanking your lucky stars that he can’t fully see the obscene blush mottling your face, you nod with difficulty against his steadfast hold on your tresses.  

“Good, but this ain’t gonna be sunshine and rainbows.  I ain’t no Care Bear.”  One last tug, and he lets go, your face falling heavily into the cushion once again.  “Don’t forget to thank your daddy.”

Without warning, another smack.  This time, there’s hardly a delay for the resonating pain.  Maybe as solid as the first, in the exact same spot, this hit rips a yelp from your throat.

Through gritted teeth, you bite out, “Thank you, Papa.” 

Leo tsks, it’s so patronizing, condescending, and strangely arousing.  “More enthusiasm, or I won’t think you’re really sorry.”

The next slap and you can feel the impact rattling your eyeballs in their sockets.

“Thank you, Papa Bear!”

“Good,” he croons, “but my fucktoy can do better.”

Oh god, you can’t stand degradation!  You hate being objectified and devalued.  Then why are you awaiting the next whack with equal measures of excitement and trepidation?  What is it about Leo that makes it acceptable for him to treat you this way?  So many possibilities, some of which may include his selfless, childish devotion to you in high school as well as the underlying threat of you being held accountable for your behaviors.  

To your own detriment, you’ve never *not* dived directly into the deep end.  Consequences be damned.

“I’m gonna have you screaming,” he asserts just as his wide palm connects violently with your opposite cheek.

“Thank you, Papa Bear!” you wail.

This goes on for seconds, minutes, hours.  Leo rains his vengeance down upon you, and you thank him for it.  You’re sobbing into the cushion, fisting the edges in a white-knuckled grip that tears the seams.  Never stopping, Leo sets your skin aflame with strong, rapid strikes.  Your only hope is that his right hand tires before you’re black and blue.

“Well, well, well,” he sings as you bawl, “look what we have here… my slut is showing her true colors.”   Fingers dip into the soaked mess of your clasped thighs.  “Only filthy whores love spankings.”

You can’t exactly deny it, feeling how puffy your pussy lips are as his fingers slide salaciously over them before delving in between to immediately drag across your sensitive clit.  A moan, more strangled than you’d care to admit, encourages him to inspect you further.  Moving his leg to release yours, he spreads you out atop him, inserting a pillow beneath your hips for better access. 

And, effectively hiding from you if he’s satisfied, if he’s turned on by your squirming and screaming.  Your submission.

His stump bruises the skin between your shoulder blades, fingers of his right hand stretching your labia open wider.  It doesn’t seem possible when the two thick digits of his right hand clamp around your clit, pinching and pulling.  Fuck, it feels so good.  You’re already on the verge of coming, he has you wound up *so* *tight*.  

“Look at this dirty hole, absolutely dripping.  You have no shame.”  His voice is still mean, even if a little awestruck.

“No,” you agree, wanting his attention and approval more than the air you breathe.

You may have made a mistake when your hips involuntarily roll up into his hand.  Fast as a gunshot, his fingers yank away.  Your disapproving groan transforms into a keen as he swats your voracious pussy.

“My brat sure is full of herself.”  Another sharp rap has your cunt convulsing, constricting on nothing.  “You really think you’re worth it?”  Your clit pulses in time with the successive beats of his fingers drumming viciously atop your pussy.  “Prove it, brat.”

Without warning, he shoves you from his lap, off the couch, and onto the floor in an ungainly heap.  You’re dazed for a second, lying on your side with a swollen pussy crushed between slick-sodden thighs.  It’s fucking criminal!  He’s got his arms crossed, leaning back into the cushions, legs spread imperiously.  Not exactly smirking, he’s only *just*, but you can tell he likes cajoling you into ruin.

“What,” you swallow around nothing, “what do you want, Papa Bear?”

Instantly, it’s like he shuts down, his expression closing off.  “This ain’t the time.  Fix those damned socks.”

Peering down, you see your knee-highs have fallen to gather around your ankles.  Sitting up, legs straight out in front of you, your eyes never leave his.  Lifting one knee, you delicately unroll the soft fabric of alternating pink and white.  When you’re done, you hesitate.  Instead of lowering your leg, you let your knee fall to the side to expose your naked body to his gaze.

His eyes drop, and you lie on your back before lifting your other leg in the air to adjust your sock.  Finished, your leg drops into place to mirror the first.  Fully bared to his hungry stare.

He growls out, “What’re you doin’, brat?”  You tempt fate, lifting your head to glance at him.  “Ah, see, I knew it was too good to be true.  You know exactly what you’re doing.  What a greedy, little fuckhole.”

It’s not the best time, but you can’t help yourself.  Your gaze drifts down his body to the tenting of his briefs.  

He notices, cocking an eyebrow.  “Something you need?”

His left arm rises to rest on the back of the couch, his right hand grasping the trunk of his cock encased in that stupid fucking underwear.  All you want is a peek, just to see him.  To know what it would look like if it was inside you *right now*.  Your mouth fills with saliva, wondering if maybe he’ll choose to come down your throat again.  You can do better.  He can fuck your throat until you *choke*.

Leaping onto your knees, heedless of your tousled appearance, you fold your hands in front of you and bat your lashes.  “Papa Bear, may I suck your cock?”  A beat passes and you hurriedly remember to say “please” and “thank you.”

Lips thinning, eyes narrowing, he doesn’t budge beyond a deft palming of his erection.  “I don’t think so.  You know what I want right now?”  Eyes wide, you inch forward on your knees.  “I want you to rub my shoulders.”

Feeling the tension drain from your limbs, you deflate.  “A massage?”

“Did I fucking stutter?”  He stands and proceeds to drop-drawers without preamble, it takes a few seconds for you to tune back into reality. 

He’s sitting back down when you inch further forward, muttering, “You have a beautiful cock.”

And, he does, how could you have forgotten?  It’s absolutely perfect.  

His thick shaft stiffly rests on his tummy in a pillow of sparse hair, lilting ever so slightly to the left.  Glans gleaming in the dim light from the window, his head like the ripe and juicy cherry atop the best dessert of your life.  Following the path of a prominent vein to the root and further, you admire his weighty testicles, dangling loosely between his legs.

Irrationally, you want to bury your nose there in the coarse pubic hair.  You almost do.

“Please, may I touch you, Papa?”

His grin is more sinister than satisfied.  “Oh, you’ll be touching me all right.  What does my cumslut want exactly?  Tell me.”

Right hand lazily stroking his magnificent shaft, he waits.  

“I,” another inch covered as you lick your lips, “I want to smell you, rub my face against you.  I want to taste you, and tongue your balls.  I want your cum in me, filling—”

“Nuh uh, you won’t like where I bury my cum inside you.”

Earnestly shaking your head, you cross a couple inches closer.  “No, daddy- Papa Bear, I want all of you.”

Raising his stump towards you, he angrily snaps, “Well, do it, then.”

You eye his arm warily, not exactly sure what he intends.

Sneering, he grumbles, “Knew it was too much for *you*.”

With that, he lets his arm drop to his side.  The action looks similar to resignation.  

Grabbing the bull by the horns, because Leo couldn’t possibly ever give up, you scale the couch on his left side.  As soon as you’re beside him, you calm yourself before tenderly, with tepid movements of your fingers, tracing the scars decorating his forearm.  They’re aged, a precious purpling of the skin that takes your breath away with its visceral artistry.  

Lifting his stump, lowering your head, he gasps as you lightly kiss every millimeter of pleated, crimped flesh.  

Hesitantly, though you don’t stop to ask permission, your tongue darts out to taste him there.  Unthinking, your gaze ascends to meet him.  He doesn’t look happy or mad, he looks… sad, maybe?  No, he looks lost.  His face is drawn in a caricature of deep-seated anguish, or longing.  Daddy’s brat is supposed to be good for him, you have to show him you’re worth it. 

That he’s worth it, too.

Guiding his arm to drape across the seat cushions, you creep his stump between your legs before lowering your body to ride his limb.  The heat of his skin is shocking against the wet that has cooled at your core.  Oh god, the way the dips and swells of his scarred skin caress your pussy has you moaning unabashedly.  

“Please, Papa Bear, please,” you beg, all coherency forgotten with the rolling waves of sensation crashing over you.

His right hand clutches your chin firmly, stopping the undulation of your hips on his stump.

“Not enough for you?” The question is barely audible, filled with what feels like a precursor for malice.  Yet, none of that bothers you.

“It’s so much, Papa.  It-it feels too good.”  His eyes search yours.  “Can you fit inside me like this?  Please?”

He huffs so powerfully that a lock of hair that had fallen over his brow flies away from his face.  “Maybe another time… if you—”

“Yes, oh god, yes, please!”  Your exclamation is emphasized with a pointed jutting of your hips, slamming your pussy into his stump with wild abandon.

Letting go of your chin, he gives you a light, full-handed tap to the face, more warning than wallop.  “Stop interrupting me.”  

Your vigor sufficiently quelled for the time being, you nod like the good girl you know you can be for your daddy.  

“Now, I specifically asked you for that shoulder rub.”

And, that’s it.  No instructions, not even as you stare at him imploringly.  

“Well, what’re you waiting for?  Be the tight, little cocksleeve I know you can be and get to it.”

Joy bubbles high in your chest as you situate yourself to straddle his lap.  He doesn’t help, just stares down between you both as you widen your stance over his massive thighs and prop his cock at your entrance to slide down.  The blunt head batters your pussy almost as much as his stump.  You definitely think you should have prepared for this, yet the discomfort of being split open arouses you more.

Never uttering a word, Leo wraps his arms around your ribs in a secure embrace before wrenching you down while his hips piston up.  

Your body ignites, sight going white as tears brim before pouring down your flushed cheeks, while he hisses through clenched teeth.  Snarling, he reminds you that any good cocksleeve knows to keep still while they warm their daddy’s cock.  It’s hardly a fucking consolation for the complete desolation of your pussy.  So inexorably destroyed to reform around his unyielding penetration.

“Yes, Papa, yes,” you mewl as your trembling hands move to grip his broad shoulders.

It’s torturous, never in your life has touching someone else been so all-consumingly impossible.  The pounding of your heart is felt deep inside you where his glans is compressed against your cervix.  It’s excruciating, hurts so badly holding still, yet the prospect of ignoring his orders and fucking has you obeying with a blind devotion to his desires.  Your hands tremulously squeeze and release, knead and abate.

Breathing comes in short pants, insufficient for air, that leave your head spinning.  So, on a deep inhale, Leo crushes you to his chest.

“What’d I fuckin’ say ‘bout not movin’, bitch?”

Staring, you’re stunned, his expression is a reflection of your own.  But, this time, you feel it when the walls of your pussy involuntarily clamp around him.  

Oddly emboldened, you blithely question, “You talk to all your girls like that?”

“Only you.”  It’s like an oath, pregnant with promise. 

Grasping for his left arm, you reach to pull his stump from around your body.  You can feel the wet spot on your back where he’d held you, the encroaching chill sending shivers up your spine.  Opening your mouth, you present your pink tongue before leaning forward to luxuriantly lick the remaining juices from his skin. 

As your tongue draws up and down, you clumsily mutter, “Thank you, Papa Bear.  That’s all I ever wanted.”

Surging forward, Leo meets your tongue with his own at the tip of one of his scars. 

He moans tasting you.  “One of these days, I’m gonna lay you out like a feast.  Eat you til you’re beggin’ me to stop.  I won’t.”

Your eyes smolder, lashes fluttering with the idea. “Never stop.”

Both of you exchange air, hot and humid.  It’s heady, and profound.  Like the interweaving of souls.  So desperate to have him, you give all of yourself.  Aggressively propelling your essence into him, he gulps you down.  And, you do the same.  Swallowing every atom that composes Leo.  

With your soft whine, his lips finally, at last, touch yours in a fleeting kiss.  

It’s enough to set the world to rights in a rotation that sees you giddy with jubilation.

The moment doesn’t last.

An animalistic growl from deep in his chest is the only signal when he bodily lifts you over him before slamming you back down.  “Gonna fill my cumslut so full,” he grunts, never losing his pace.

“That’s right, filthy whores like you get used.  You love it.  Perfect cumdumpster.”  Each pause is punctuated with a particularly harsh bite into the fragile flesh of your throat.  As his teeth sink in, you tear your nails down his back.

“Fuck, you shouldn’t have done that,” he blusters.

Abruptly, you're on your back, legs being manipulated above your head to curtain his shoulders while your arms are stuffed beneath your back.  He drives his cock blindly inside you, stabbing in and out without remorse the whole time.  Once he has you securely beneath him, his right hand winds around your throat.  Coiling tighter, your vision narrows as fat bubbles of black pop prettily before your eyes.

“Gonna paint the inside of your nasty pussy.  Write my name on the walls.  No one’s ever gonna use you the way I can.”  

Words barely registering in your mind, and unable to give your voice flight, his grunts occupy the space recently vacated between your ears.  Pounding into you, he thumbs at your bottom lip.  Prying your mouth open, his expression darkens.  Licking his lips, you hope he’ll kiss you.  He gives you something better.

Working his tongue, saliva pooling in his mouth, you’re entranced by a plump puddle of spittle gathering into a dense rope before the force of his lunging sees it break to fall past your gaping maw.

A bittersweet syrup, almost too hard to swallow, but you revel in it.

“Please,” you squeak.

Panting, he stumbles over his words.  “W-want me to f- *fuck*— fill you?  This mouth be- belongs to me.  This pussy is- is mine.  And,” he groans, “later,” pausing to make sure your eyes are locked with his, “so is this ass.”

Suddenly, his right hand is gripping your breast, fingers tugging at your nipple as his head falls back to your shoulder.  Left limb slipping around your shoulders, he rears up on his haunches with you in tow.  You’re impaled mid-air, his cock and one arm the only anchors keeping you aloft.  He uses you like a fleshlight, pumping you on and off, up and down.  

His rhythm speeds up into a stuttering staccato that has the deep burn flashing like a signal flare.

“Say it,” he spits into your ear.

“I-I-I—,” you’re mindless, a blank canvas, ready for him to stain you in his varnish.  

“Yours,” is the best you can do, howling your new mantra back at him as he continues to reprimand you, his pace only increasing in speed and intensity.

“My breeding slut,” he roars, “can't wait to breed and milk you like my perfect whore deserves. Your ripe womb is greedy for me.”

Your body no longer yours, you soar into the heavens on a silent scream as his cum splashes virulently inside you until it overflows to spill onto the ruined cushions...

Daily letters and notes will pack your tiny planner, detailed instructions carefully read and followed.

Weeks will go by, calls will be answered before the third ring.  It may be an unknown number, yet you’ll always answer. 

Months will roll by, days both lazy and difficult will be spent in sublime ecstasy.  Papa Bear and his brat will savor every second. 

Years will pass…
