---
layout: page
title: "Privacy Policy"
date: 2022-07-16
---
Canonicity (C8y) is run by two individuals, Ariadne and Sisyphus, who are committed to privacy, especially yours. This Privacy Policy governs C8y and any information submitted to us. We do not collect, process, and/or retain any user data at this time, nor is it planned for the future.

This Privacy Policy applies only to this website. C8y and users may provide links to or content via sites that are owned or controlled by third parties, and may use such sites to communicate information about C8y. C8y has no control over third-party sites or their terms of use or privacy policies, and you agree that C8y is not responsible for and does not endorse their content, terms, or availability. If you follow links off C8y, you should review those sites' privacy policies, which may be different, and for which C8y takes no responsibility.

If this Code of Conduct and/or Privacy Policy change at any point in the future, we will post the changes to the corresponding pages. Such changes will be used only for information provided by those who have visited, used, or accessed C8y after the effective date of such policy changes. If you are concerned about how your information is used, you should check back at the C8y site periodically to review our policies.

If you feel that C8y is not following the stated policies, please [Contact](mailto:meta@canonicity.org) us.