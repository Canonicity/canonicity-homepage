---
layout: work_index

title: Maiden-Heretic of Laclathan

author: Ariadne

social: https://mastodon.canonicity.org/web/@ariadne

work_category: tmhol

category: indices

oneshot: false

permalink: /works/TMHOL

tagline: Saki Kayenlora's died twice already. Sinóle was an orphan until Saki's sworn enemies took her in. Cursed with the talents of Superheavy Exo pilots, together they're determined to make their mark on a universe stranger and larger than anything they've ever imagined.

summary: Unstoppable life both born and made stampedes across the Known Universes. All attempts to control this phenomena called TTMA have failed. She grows as she has for eight-hundred thousand years, tended by the law-consuming gardeners who revel in her flow/:/ The Arcaelits. Enter Saki Kayenlora (she’s died twice already!) and Sinóle, a former child soldier and refugee both cursed with the talents of Superheavy Exo pilots. Welcomed by the Arcaelits from doomed cultures, Saki and Sinóle learn to live again in their whorling, stupefying new world the only way they know/:/ competitive training for Superheavy Exo combat. The mission? Avoid millennia of war by highjacking a performance art apocalypse… and kill an entity some four trillion people fervently believe is god.

rating: Mature Audiences

warnings: [Violence]

tags: [Ongoing, Novel, Science Fiction, Mecha, Action/Adventure]

---
Unstoppable life both born and made stampedes across the Known Universes. All attempts to control this phenomena called TTMA have failed. She grows as she has for eight-hundred thousand years, tended by the law-consuming gardeners who revel in her flow: The Arcaelits.

Enter Saki Kayenlora (she’s died twice already!) and Sinóle, a former child soldier and refugee both cursed with the talents of Superheavy Exo pilots. Welcomed by the Arcaelits from doomed cultures, Saki and Sinóle learn to live again in their whorling, stupefying new world the only way they know: competitive training for Superheavy Exo combat.

The mission? Avoid millennia of war by highjacking a performance art apocalypse… and kill an entity some four trillion people fervently believe is god.

