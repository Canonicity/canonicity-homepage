---
layout: work_index

title: All's Fair

author: Sisyphus

work_category: allsfair

category: indices

permalink: /works/AllsFair

tagline: Growing up, he was only a friend. You’ve made lots of mistakes, and now you’re getting your comeuppance from a man who **could** care less.

rating: Mature Audiences

warnings: [Sexual Content, Rape/Non-Con]

tags: [Alternate Universe, Short Story]

---
