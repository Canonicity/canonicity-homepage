I"�
<h2 id="code-of-conduct">Code of Conduct</h2>

<p>In Canonicity (C8y), this is an unofficial agreement to engage yourself, others, and environments with respect.  That’s it; the only rule is respect.  </p>

<p>While that seemed simple enough, it was a pretty open-ended concept when we drafted this document.  For us, respect is too broad and vague in terms of cultural interpretation, not to mention individual perspectives and perceptions.  </p>

<p>Instead of asking for our personal ideals, we’d like to promote collective positive freedoms by providing negative individual prohibitions.  In other words, a living code or guidelines of what we expect people <strong>NOT</strong> to do in our community.  Our goal is to include the spirit of the code within the letter of the code to preserve our individual perspectives on the nature of subject matters while promoting a sustainable community built on collaboration and accessibility. </p>

<p>Users will <strong>NOT</strong>:</p>

<ol>
  <li>
    <p>Discriminate against any individual based on any bias or prejudice</p>
  </li>
  <li>
    <p>Intentionally foster and/or exacerbate negativity counterproductive to equity</p>
  </li>
  <li>
    <p>Impose on the personal boundaries of any individual or environment</p>
  </li>
  <li>
    <p>Participate in implicit or explicit expressions of bad faith</p>
  </li>
  <li>
    <p>Nullify or invalidate personal agency and/or accountability</p>
  </li>
  <li>
    <p>Break the law, Creepo!</p>
  </li>
</ol>

<h2 id="at-will-participation">At-Will Participation</h2>

<p>Because users retain their agency and accountability, we hope that C8y will be well-equipped to self-regulate, but we know this isn’t always possible.  If one or more of these prohibitions is violated, please <a href="mailto:meta@canonicity.org">Contact</a> us to work together to address the issue on an individual case-by-case basis.</p>

<p>When appropriate, it is up to the discretion of C8y to act within reason to protect the fidelity of this living experiment or thought project.  C8y will not publicly address or publish information regarding any individual(s) involved. </p>

<h2 id="form-of-the-user">Form of The User</h2>

<p>So, we have a saying that “tools should take the form of the user,” and we feel the same now.  </p>

<p>Because C8y is living, C8y is constantly growing and evolving.  As individuals, we know how rare it is to grow up in true isolation identical to a social vacuum–nearly impossible!  Therefore, we’d like your help in raising C8y.  We feel that C8y deserves your voice, input, and care.</p>

<p>Feel free to join our <a href="https://forum.canonicity.org" target="_blank">Forum</a> with your thoughts, ideas, and suggestions.</p>
:ET