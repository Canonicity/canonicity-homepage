---
layout: frontpage
title: "Home"
date: 2022-07-01
---
## What We're Doing

We're building an open-content fictional world to support transformative acts of storytelling and art outside of exclusionary frameworks of ownership or hierarchy.  We know how hard fans and creators work, and our upcoming commercial License will allow individuals and self-funded teams to reserve copyright during a decently-sized launch window while related (so-called) intellectual property stays unencumbered. We hope Project Canonicity becomes a cultural alternative where individuals are free to customize their learning and livelihoods on their own terms. 

## How We Plan to Do It

Our stories so far can be found in [Works](works/), please be advised some content is intended for mature audiences. For more about our world, we have a burgeoning [Wiki](https://wiki.canonicity.org){:target="_blank"}.

## Why We Do It This Way

Project Canonicity is a living experiment and its online presence is subject to change at any time to encourage progress and advancement, professionally and personally. We know that this can be an inconvenience, therefore we will do our best to remain transparent about what revisions are made and when. If you have any questions or concerns, please feel free to [Contact us](mailto:{{ site.email }}).
